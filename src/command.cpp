/**
 * @author NG TING FUNG 1155030851
 */
#include <cstdio>
#include <iostream>
#include <string>
#include <vector>
#include <iterator>
//#include <regex>
#include <map>
#include <cstring>


#include <exception>
#include "command.h"

using namespace std;

using namespace Shell;

/**
command Object
- command String
- tokens Token Object
*/
Command::Command(char * cmd) {

	this->text = string(cmd);
	this->tokens.reserve(5);

	this->tokenize();
	this->validiate();
}
Command::Command(string cmd) {
	this->text = cmd;
	this->tokenize();
	this->validiate();
}
/**
* Tokenize the input with [spacebar]
*/
void Command::tokenize() {
	char *str, *token, *saved;
	int i;
	struct token previous;
	previous.type = TOKEN_START;
	//convert c++ string to c char*
	char * cmd = new char[this->text.length() + 1];
	std::strcpy(cmd, this->text.c_str());

	for (i = 1, str = cmd;; i++, str = NULL) {
		token = strtok_r(str, " ", &saved);
		if (token == NULL)
			break;
		struct token t;
		t.text = token;
		t.type = TOKEN_UNDEFINED;

		if ((int) strlen(t.text) == 1) {
			switch (t.text[0]) {
			case '>':
				t.type = TOKEN_REDIRECT_OUTPUT;
				break;
			case '<':
				t.type = TOKEN_REDIRECT_INPUT;
				break;
			case '|':
				t.type = TOKEN_PIPE;
				break;

			}
		} else if (strcmp(t.text, ">>") == 0) {
			t.type = TOKEN_REDIRECT_OUTPUT;
		}

		if (previous.type != TOKEN_START && t.type == TOKEN_UNDEFINED
			&& previous.type != TOKEN_PIPE) {

				if (previous.type == TOKEN_REDIRECT_INPUT) {
					t.type = TOKEN_REDIRECT_INPUT_FILENAME;
				} else if (previous.type == TOKEN_REDIRECT_OUTPUT) {
					t.type = TOKEN_REDIRECT_OUTPUT_FILENAME;
				} else {
					t.type = TOKEN_ARGUMENT;
				}

		} else if (strcmp(t.text, "cd") == 0 || strcmp(t.text, "fg") == 0
			|| strcmp(t.text, "exit") == 0 || strcmp(t.text, "jobs") == 0) {
				t.type = TOKEN_COMMAND_BUILTIN;
		} else if (t.type == TOKEN_UNDEFINED) {
			t.type = TOKEN_COMMAND;
		}

		previous = t;
		this->tokens.push_back(t);
	}
	//t.type = TOKEN_COMMAND;

	//	printf("     [TOKEN %d]: %s\n",t.type, token);

}

/**
* validate tokens
*/
void Command::validiate() {
	bool isBuiltInCmd = false;
	bool hasInput = false;
	bool hasTerminate = false;
	int hasPipe = 0;
	struct token t;
	struct token previous;
	previous.type = TOKEN_START;
	previous.text = NULL;

	for (int j = 0; j < this->tokens.size(); j++) {
		t = this->tokens[j];

		//Spec p.6
		if( previous.type == TOKEN_START && (t.type != TOKEN_COMMAND && t.type != TOKEN_COMMAND_BUILTIN)){
			//should start with command or builtin command
			throw "the first token is not a command";
		}else if( previous.type != TOKEN_START && t.type == TOKEN_COMMAND_BUILTIN){
			//[start] := [built-in command] [arg]*;
			throw "built-in command not first token";
		}else if (t.type == TOKEN_COMMAND_BUILTIN) {
			isBuiltInCmd = true;
		} else if (isBuiltInCmd && t.type != TOKEN_ARGUMENT) {
			//[built-in command] [arg]*; only accept argument if we have built in command token
			/*	printf("[DEBUG]Unexpected token %s (Type %d) after %s (Type %d)\n",t.text, (int) t.type, previous.text, (int) previous.type);*/
			throw "unexpected token after built in command";
		} else if (hasTerminate && t.type == TOKEN_PIPE) {
			// if we have [terminate] , we can't have pipe / [recursive]
			/*	printf("[DEBUG]Unexpected token %s (Type %d) after %s (Type %d)\n",
			t.text, (int) t.type, previous.text, (int) previous.type);*/
			throw "unexpected token pipe after terminate";
		} else if ( ( ((hasTerminate || (hasInput && !hasPipe))
			&& t.type == TOKEN_ARGUMENT)) || (previous.type != TOKEN_COMMAND_BUILTIN && previous.type !=  TOKEN_COMMAND  && previous.type != TOKEN_ARGUMENT && t.type == TOKEN_ARGUMENT)) {
				//[command] := [command name] [args]*
				//no argument after accepted a terminate or input and without pipe
				//or argument can only appear after command or built in command or argument
				throw "unexpected argument after terminate or input";
		} else if (hasTerminate && t.type == TOKEN_REDIRECT_OUTPUT) {
			//no duplicate output
			throw "unexpected token: redeclare redirect output ";
		} else if (hasInput && t.type == TOKEN_REDIRECT_INPUT) {
			//no duplicate input
			throw "unexpected token: redeclare redirect input ";
		} else if (hasPipe && t.type == TOKEN_REDIRECT_INPUT) {
			//no input after any pipe
			throw "unexpected redirect input after pipe";
		} else if (hasPipe >= 2 && t.type == TOKEN_PIPE) {
			//max pipe = 2
			/*
			Pipe restriction: in order to ease your pain in implementing this assign-
			ment, we restrict the maximum number of pipes, i.e., ��|��, in a command
			line to 2 only.
			*/
			throw "too many pipes";
		} else if (previous.type == TOKEN_REDIRECT_INPUT && t.type != TOKEN_REDIRECT_INPUT_FILENAME ) {
			//redirect input filename only after redirect input
			throw "invalid redirect input";
		} else if(previous.type == TOKEN_REDIRECT_OUTPUT && t.type != TOKEN_REDIRECT_OUTPUT_FILENAME){
			//redirect outpu filename only after redirect output
			throw"invalid redirect output";
		}

		if (t.type == TOKEN_REDIRECT_INPUT) {
			hasInput = true;
		} else if (t.type == TOKEN_REDIRECT_OUTPUT) {
			hasTerminate = true;
		} else if (t.type == TOKEN_PIPE) {
			hasPipe++;
		}
		//[><|*!������]
		//check invalid character
		//	std::regex regex_command("[0-9]+", std::regex_constants::basic);
		//	strcspn
		//	try{
		//	printf("Regex %d", (int) regex_search( t.text , regex_command) );

		// ASCII 96 `  39 '   34  \"   33 !   42 *    62 > 60 <  124 |
		char command_invalid[] = "><|*!`'\"\t";
		char args_invalid[] = "><|!`'\"\t";
		char filename_invalid[] = "><|*!`'\"\t";
		int pos;
		// Therefore, the function will return the length of str1 if none of the characters of str2 are found in str1.
		if(t.type == TOKEN_COMMAND && strcspn( t.text , command_invalid) != strlen(t.text) ){
			throw "invalid TOKEN_COMMAND";
		}
		if(t.type == TOKEN_ARGUMENT && strcspn( t.text , args_invalid) != strlen(t.text) ){
			throw "invalid TOKEN_ARGUMENT";
		}	
		if((t.type == TOKEN_REDIRECT_INPUT_FILENAME || t.type == TOKEN_REDIRECT_OUTPUT_FILENAME) && strcspn( t.text , filename_invalid) != strlen(t.text) ){
			throw "invalid TOKEN_REDIRECT_INPUT_FILENAME or TOKEN_REDIRECT_OUTPUT_FILENAME";
		}

		previous = t;
		//throw "validator not implemented";
	}

	if (previous.type == TOKEN_PIPE || previous.type == TOKEN_REDIRECT_INPUT
		|| previous.type == TOKEN_REDIRECT_OUTPUT) {
			throw "unexpected end of token";
	}

}
