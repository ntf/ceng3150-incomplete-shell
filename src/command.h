#ifndef COMMAND_H
#define COMMAND_H

#define TOKEN_UNEXPECTED -2
#define TOKEN_UNDEFINED -1
#define TOKEN_START 0
#define TOKEN_EMPTY 10
#define TOKEN_COMMAND_BUILTIN  1
#define TOKEN_COMMAND  2
#define TOKEN_ARGUMENT 3
#define TOKEN_REDIRECT_INPUT 4
#define TOKEN_REDIRECT_OUTPUT 5
#define TOKEN_REDIRECT_OUTPUT 5
#define TOKEN_REDIRECT_INPUT_FILENAME 6
#define TOKEN_REDIRECT_OUTPUT_FILENAME 7
#define TOKEN_PIPE 8
namespace Shell{

	struct token {
		int type;
		char* text;
	};

	class Command{

	public:
		std::vector<token> tokens;
		std::string text;


		Command(char * cmd);
		Command(std::string cmd);

	protected:
	void tokenize();
	void validiate();
	};
}
#endif
