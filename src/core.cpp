/**
* @author NG TING FUNG 1155030851
* @author MOK LONG YAT 1155029363
*/
#include <cstdio>
#include <cstdlib>
#include <csignal>
#include <cerrno>

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <cstring>
#include <exception>
#include <glob.h>

#if defined(WIN32)
#ifndef _UNISTD_H  
#define _UNISTD_H  
#include <io.h>  
#include <process.h>  
#endif /* _UNISTD_H */  
#else
#include <unistd.h>
#endif

#include "command.h"
#include "process.h"
#include "core.h"

using namespace std;
using namespace Shell;

void signal_handler_blank(int signal){

}
/**
*@constructor
*/
Core::Core() {
	//signal handler
	//handle Ctrl+C 
	/* Spec p.18
	SIGINT Ignore the signal.
	SIGTERM Ignore the signal.
	SIGQUIT Ignore the signal.
	SIGTSTP Ignore the signal.
	SIGSTOP Default signal handling routine.
	SIGKILL Default signal handling routine.
	*/
	signal(SIGINT,SIG_IGN); 
	signal(SIGTERM,SIG_IGN); 
	signal(SIGQUIT,SIG_IGN);
	signal(SIGTSTP,SIG_IGN); //@see ProcessManager suspend
	signal(SIGSTOP,SIG_DFL);
	signal(SIGKILL,SIG_DFL);
	if (getcwd(this->workingDir, sizeof(this->workingDir)) == NULL) {
		printf("getcwd() error\n");
	}
	this->pm = new ProcessManager();
}

Request Core::console() {
	Request req;
	string input;
	if (getcwd(this->workingDir, sizeof(this->workingDir)) == NULL) {
		printf("getcwd() error\n");
	}

	printf("[3150 Shell:%s]$ ", this->workingDir);

	input = this->input();
	Core::checkEOF(true);
	req.input = input;
	return req;
}
/**
* handle Ctrl+D (EOF)
*/
bool Core::checkEOF(bool exitNow ){
	if(std::cin.eof()){

		if(exitNow){
			printf("\n");
			std::exit(0);
		}
		return true;
	}
	return false;
}
string Core::input() {
	string text;

	getline(std::cin, text);
	return text;
}

void Core::handle(Request* req) {
	//phase1
	//printf("----PHASE1----\n");
	//this->phase1(req);
	//printf("----PHASE2----\n");

	if(req->commands.size() <= 0){
		return;
	}
	//phase2

	Process* ps = NULL;
	Process* previous = NULL;
	Process* temp = NULL;
	bool found = false;//command found?
	//2.3 Process creation
	//2.3.1 Finding the programs
	// path search order :  /bin ! /usr/bin ! . (current directory)
	Command* cmd = &req->commands[0];
	char realPath[256] = "\0";
	char accessPath[256] = "\0";
	for (int j = 0; j < cmd->tokens.size(); j++) {
		/*printf("Token %d: \"%s\" (%d)\n", j + 1,
		cmd->tokens[j].text,
		cmd->tokens[j].type);*/
		switch(cmd->tokens[j].type){
		case TOKEN_COMMAND_BUILTIN:
			Core::built_in_command(req);
			return;
			break;
		case TOKEN_COMMAND:

			//If a path��s name begins with the character ��/��, then that path is an absolute path.
			//Names starting with ��./�� and ��../��
			if(strncmp(cmd->tokens[j].text,"/",1) == 0|| 
				strncmp(cmd->tokens[j].text,"./",2) == 0 || strncmp(cmd->tokens[j].text,"../",3) == 0){
					//printf("Absolute path \n");
					//found = true;//let exec* to check
					strcat(realPath,cmd->tokens[j].text);
					found = true;
					/*
					strcpy(accessPath,this->workingDir);
					strcat(accessPath,cmd->tokens[j].text);
					printf("target: %s ; %s\n",realPath,accessPath);
					if(access( accessPath , F_OK ) == -1){
					found = true;
					} */
			}else{
				// /bin ! /usr/bin ! . (current directory)
				//relative path
				char directories[3][20] = { "/bin" , "/usr/bin" , "./"};
				//printf("Relative path\n");
				for(int i = 0 ; i < 3; i++){
					strcpy(realPath,directories[i]);
					strcat(realPath,"/");
					strcat(realPath,cmd->tokens[j].text);
					if(access( realPath , F_OK ) != -1){
						//file exists
						found = true;
						break;
					} 
				}
			}


			if(found){
				if(ps == NULL){
					ps = new Process();
					previous = ps;
					temp = ps;
				}else{
					temp = new Process();
					previous->next = temp;
					previous = temp;

				}
				temp->command = new char[cmd->text.length() + 1];
				std::strcpy(temp->command, cmd->text.c_str());

				temp->exec = new char[ strlen(realPath) +1];
				strcpy(temp->exec,realPath);
				temp->cmd = cmd->tokens[j].text;
			}else{
				printf("%s: command not found\n",cmd->tokens[j].text);
			}
			break;
		case TOKEN_ARGUMENT:
			//2.3.2 Wildcard expansion
			if(temp){
				//do wildcard expansion here
				string q;
				q = string(cmd->tokens[j].text);
				if(q.find('*')!= -1){
					glob_t glob_result;
					int source;
					string p;

					source = glob(cmd->tokens[j].text, 0, NULL, &glob_result);
					if (source == 0){
						for(int i=0;i<glob_result.gl_pathc;++i){
							token t;
							t.type = TOKEN_ARGUMENT;
							t.text = glob_result.gl_pathv[i];
							temp->tokens.push_back(t);
						}
					}else{
						//not found or error or out of memory
						//When you cannot expand a particular wildcard expression, just keep the expression unexpanded (as shown in the last example).		
						temp->tokens.push_back(cmd->tokens[j]);
					}
				}
				else{
					temp->tokens.push_back(cmd->tokens[j]);
				}
			}


			break;

		case TOKEN_PIPE:

			break;

		case TOKEN_REDIRECT_INPUT_FILENAME:
			if(temp){ temp->input = cmd->tokens[j].text;}
			break;


		case TOKEN_REDIRECT_OUTPUT_FILENAME:
			if(temp){ 
				temp->output = cmd->tokens[j].text;
			}
			break;
		case TOKEN_REDIRECT_OUTPUT:
			if(temp){ 
				temp->outMode = strcmp(cmd->tokens[j].text, ">>") == 0 ? 1 : 0;
			}
			break;
		}

	}

	//Process Object constructed
	if(ps){
		this->pm->execute(ps);
	}

}
void Core::phase1(Request* req) {
	/*
	Redirect Input Redirect Output Built-in Command Argument
	Pipe Command Name Input Filename Output Filename
	*/
	map<int, char*> tokensMap;
	tokensMap[ TOKEN_COMMAND_BUILTIN] = "Built-in Command";
	tokensMap[ TOKEN_COMMAND] = "Command Name";
	tokensMap[ TOKEN_REDIRECT_INPUT] = "Redirect Input";
	tokensMap[ TOKEN_REDIRECT_OUTPUT] = "Redirect Output";
	tokensMap[ TOKEN_ARGUMENT] = "Argument";
	tokensMap[ TOKEN_REDIRECT_INPUT_FILENAME ] = "Input Filename";
	tokensMap[ TOKEN_REDIRECT_OUTPUT_FILENAME ] = "Output Filename";
	tokensMap[ TOKEN_PIPE] = "Pipe";

	for (int i = 0; i < req->commands.size(); i++) {
		//	cout << "CMD: " << req->commands[ i ].text << "\n";
		for (int j = 0; j < req->commands[i].tokens.size(); j++) {
			printf("Token %d: \"%s\" (%s)\n", j + 1,
				req->commands[i].tokens[j].text,
				tokensMap[ req->commands[i].tokens[j].type]);
		}
	}
}
/* Here start lemon's coding for built_in commands */
/**
* Built in command: CD
*/
void Core::built_in_command(Request* req){
	map<int,string> built_in_commands;
	built_in_commands[0] = "cd";
	built_in_commands[1] = "exit";
	built_in_commands[2] = "fg";
	built_in_commands[3] = "jobs";
	string cmd;
	string built_in;

	built_in = req->commands[0].tokens[0].text;
	if(built_in.compare(built_in_commands[0])==0){

		if(req->commands[0].tokens.size()>2){
			cout << "cd: wrong number of arguments\n";
		}else{
			cmd = req->commands[0].tokens[1].text;

			//do wildcard expansion here
			string q;
			q = string(cmd);
			if(q.find('*')!= -1){
				glob_t glob_result;
				int source;
				string p;
				source = glob(req->commands[0].tokens[1].text, 0, NULL, &glob_result);
				if (source == 0){
					if( glob_result.gl_pathc > 1){
						cout << "cd: wrong number of arguments\n";
						return;
					}else if(glob_result.gl_pathc == 1){	
						cmd = glob_result.gl_pathv[0];	
					}
				}else{
					//not found or error or out of memory
					//When you cannot expand a particular wildcard expression, just keep the expression unexpanded (as shown in the last example).		

					//blank
				}
			}

			if(chdir(cmd.c_str()) == -1){
				cout << "[arg]: cannot change directory\n";
			}
		}
	}else if(built_in.compare(built_in_commands[1])==0){
		if(req->commands[0].tokens.size()>1){
			cout << "exit: wrong number of arguments\n";
		}else{
			if(this->pm->getSuspendedList().size()>=1){
				cout << "There is at least one suspended job\n";
			}else{
				exit(0);
			}
		}
	}else if(built_in.compare(built_in_commands[2])==0){
		if(req->commands[0].tokens.size()==1 || req->commands[0].tokens.size()>2){
			cout << "fg: wrong number of arguments\n";
		}else{
			// code to start suspended job
			int pos = atoi(req->commands[0].tokens[1].text);
			this->pm->setForeground(pos);
		}
	}else if(built_in.compare(built_in_commands[3])==0){
		if(req->commands[0].tokens.size()>1){
			// need to throw or not?? ask TA
			cout << "jobs: wrong number of arguments\n";
		}else{
			std::vector<Shell::Process*> jobs = this->pm->getSuspendedList();
			if(jobs.size() == 0){
				printf("No suspended jobs\n");
			}else{
				for(int i = 0; i<jobs.size(); i++){
					cout << "[" << (i+1) << "] " << jobs[i]->command << "\n";//get the list
				}
			}

		}
	}
}

/**
* parse cli input text
*/
bool Request::parse() {

	char * input = new char[this->input.length() + 1];
	std::strcpy(input, this->input.c_str());
	char *str, *command;
	struct cmd* temp = NULL;
	struct cmd* previous = NULL;
	this->commands.reserve(3);
	try {

		char *tok, *saved;
		str = input;
		/*for (command = strtok_r(str, "|", &saved); command; command = strtok_r(NULL, "|", &saved))
		{
		if(command == NULL)
		break;
		//printf("[CMD] %s \n",command);*/
		Command cmd(str);
		this->commands.push_back(cmd);
		//	}

	} catch (const char* e) {
		//printf("[DEBUG] exception: %s\n", e);
		printf("Error: invalid input command line\n");
		return false;
	}

	return true;
}
