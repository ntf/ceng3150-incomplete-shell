#ifndef SHELL_H
#include <vector>
#include "command.h"
#include "process.h"
#define SHELL_H

namespace Shell{

	/*struct command{
		char* command;
	};*/

	class Request
	{
	public:
		std::string input;
		std::vector<Shell::Command> commands;
		bool parse();
	};

	class Core {
		int x, y;
		
	public:
		ProcessManager* pm;

		Core();
		Request console();
		//void registerFn( /* register Fn */);
		bool checkEOF(bool exitNow = true);
		void handle(Request* req);

		void phase1(Request* req);
		void built_in_command(Request* req);

		std::string input();
	protected:
		char workingDir[256];
		//void tokenize();
	};




}
#endif
