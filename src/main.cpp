/**
 * @author NG TING FUNG 1155030851
 */
#include<iostream>
#include <string>
#include "core.h"

using namespace std;

int main() {

	Shell::Core core;
	Shell::Request req;

	while (1) {
		req = core.console();
		//core.isEOF(&req,true);

		//std::cout << "[DEBUG]" << req.input << "\n";
		if (req.parse()) { 
			//if no error on input command
			core.handle(&req);
		}
	}
	return 0;
}
