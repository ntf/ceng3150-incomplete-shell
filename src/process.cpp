/**
* @author NG TING FUNG 1155030851
* @author MOK LONG YAT 1155029363
*/

#include <cstdio>
#include <cstdlib>

#include <csignal>
#include <cerrno>
#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <iterator>
#include <map>
#include <cstring>
#include <exception>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#if defined(WIN32)
#ifndef _UNISTD_H  
#define _UNISTD_H  
#include <io.h>  
#include <process.h>  
#endif /* _UNISTD_H */  
#else

#include <unistd.h>
#include <sys/wait.h>
#endif



#include "command.h"
#include "process.h"

using namespace std;
using namespace Shell;

ProcessManager* instance = NULL;
void _suspend(int sig);
/*
void handleInterrupt(int sig){
	
	int pid = getpid();
	if(instance != NULL){
		printf("interrupted shell pid %d : %d\n",instance->shellPID, pid);
		if(instance->shellPID != pid){
			cout << "\n";
		}
	}
	//signal(SIGINT, SIG_DFL);
	
	//kill(pid,SIGINT);
	//signal(SIGINT,handleInterrupt);
	
	
};*/
Process::Process(){
	this->input = NULL;
	this->output = NULL;
	this->next = NULL;
	this->previous = NULL;
	this->time = 0;
	this->pipeIn = 0;
	this->pipeOut = 0;
	this->outMode = 0;
	this->isFrontProcess = false;
}
int Process::execute(ProcessManager* pm){

	bool hasRedirectError = false;

	this->arguments = new char*[ this->tokens.size() + 2];
	this->arguments[0] = this->exec ;
	for (int j = 0; j < this->tokens.size(); j++) {
		this->arguments[ j + 1 ] = this->tokens[j].text;
	}
	this->arguments[ this->tokens.size() + 1] = NULL;
	int in_fd = 0,out_fd = 0 , fd[2];
	//redirect input and output
	if( this->input != NULL){
		//printf("[DEBUG]infile open %s\n",this->input);
		in_fd = open ( this->input , O_RDONLY ) ;
		if ( in_fd == -1) {
			printf("[%s]: no such file or directory\n",this->input);
			hasRedirectError = true;
			//return 0;
		}
	}

	if( this->output != NULL){
		/* outMode = 0 for truncate , 1 for append */
		if ( this->outMode )
			out_fd = open ( this->output , O_WRONLY | O_CREAT
			| O_APPEND , S_IRWXU ) ;
		else
			out_fd = open ( this->output , O_WRONLY | O_CREAT
			| O_TRUNC , S_IRWXU ) ;
		if ( out_fd == -1) {
			printf("[%s]: permission denied\n",this->output);	
			//return 0;
		}
	}

	if(hasRedirectError){
		//Mole Wong: The first process will not execute which is the same handling in the simple redirection case. However, as we didn't define how to handle this case, you are free to handle what the remaining processes should do.
		//we skip the first process here
		//if we have remaining processes , we skip the first process
		if(this->next != NULL){
			pm->execute( this->next);
		}
		return 0;
	}

	//pipe
	if(this->next != NULL){
		
		int fd [2];
		if ( pipe ( fd ) == -1) {
			printf("pipe error\n");
		}
		this->pipeOut = fd[1];
		this->next->pipeIn = fd[0];
		this->next->previous = this;
	}
	//printf("pipeIn : %d , pipeOut : %d; hasNext: %d\n",this->pipeIn,this->pipeOut, this->next != NULL);

	
	int pid = fork();
	int status;
	if(pid == -1){
		//error
		printf("Fork Error\n");
	}else if(pid == 0){
		
		signal(SIGINT,SIG_DFL); 
		
		signal(SIGTERM,SIG_DFL); 
		signal(SIGQUIT,SIG_DFL);
		signal(SIGTSTP,SIG_DFL);
		signal(SIGSTOP,SIG_DFL);
		signal(SIGKILL,SIG_DFL);
		//signal(SIGINT,handleInterrupt); //SIGINT
		signal(SIGTSTP, _suspend);
		//0 = STDIN_FILEINFO
		if( in_fd && dup2 ( in_fd , 0) == -1){ 
			//printf("infile dup2 error\n:");/* error */
			}
		if ( in_fd &&  close ( in_fd ) == -1) {
			//printf("infile close error\n");/* error */
			}
		if (out_fd && dup2 ( out_fd , 1) == -1) {/* error */}

		if (this->pipeIn && dup2 (this->pipeIn  , 0) == -1 ) {/* error handling */}
		if (this->pipeOut && dup2 (this->pipeOut  , 1) == -1 ) {/* error handling */}
		if(this->pipeIn){
			close(this->pipeIn);
		}
		if(this->pipeOut){
			close(this->pipeOut);
		}
		//child
		if(execv(this->exec,this->arguments) == -1){						
			if(errno == ENOENT){
				/* no such file or directory */
				printf("%s: command not found\n",this->cmd);
			}else{
				printf("%s: unknown error\n",this->cmd);
			}
		}
		exit(0);
	}


	if (out_fd && close ( out_fd ) == -1) {/* error */}

	//close both pipe on parent
	if(this->pipeIn)	close(this->pipeIn);
	
	if(this->pipeOut)	close(this->pipeOut);
	//parent
	this->pid = pid;
	//printf("executing the process [%s] %d\n",this->exec,pid);
	//pm->running = this;
	
	if(this->next!= NULL){
		pm->executeInternal( this->next);
	}

	this->handleSignals(pm);

}

void Process::handleSignals(ProcessManager* pm){
	int status;
	//handle signals
	//waitpid(pid,&status,0);
	//printf("handler start [%d] \n",this->pid);
	if ( waitpid ( this->pid , &status , WUNTRACED ) != this->pid ) {
		/* error handling */
	} else {
		if ( WIFSTOPPED ( status ) ) {
			/* The child is stopped */
			pm->handleSuspend(this);

		} else if ( WIFEXITED ( status )){
			pm->handleExit(this);
		}
		else if( WIFSIGNALED(status) && (WTERMSIG(status) == SIGKILL || WTERMSIG(status) == SIGPIPE) ){
			//cout << "\n";
			//printf("SIGKILL||SIGPIPE ed [%d]\n",this->pid);
			
			if(this->previous != NULL){
			//	printf("sending SIGPIPE from %d to %d\n",this->pid ,this->previous->pid);
				kill(this->previous->pid, SIGPIPE);
			}

		} else if( WIFSIGNALED(status) ){
			cout << "\n";
		}


	}
}


void _suspend(int sig) {
	if(instance != NULL){
		instance->suspend(sig);
	}
	/*
	if (active.pid != 0) {
	kill(active.pid, SIGTSTP);
	}

	*/
}
ProcessManager::ProcessManager(){
	instance = this;
	this->running = NULL;
	this->shellPID = getpid();
	signal(SIGTSTP, _suspend);
	//signal(SIGINT,handleInterrupt);


}

void ProcessManager::handleSuspend(Process* ps){
	//printf("[%d] %s suspend.\n",ps->pid , ps->exec);
	if(ps->isFrontProcess){
		this->suspended.push_back( ps );
		this->running = NULL;
	}
}

void ProcessManager::handleExit(Process* ps){
	//printf("[%d] %s exit.\n",ps->pid , ps->exec);
	this->suspended.erase(std::remove(this->suspended.begin(), this->suspended.end(), ps), this->suspended.end()); 

}


void ProcessManager::suspend(int sig){
	if( this->running != NULL){
		Process* ps = this->running;
		Process* temp = ps->next;
		//printf("[DEBUG] receive signal suspend , suspend [%s][%d] now\n",this->running->exec,this->running->pid);
		
		//this->suspended[ ps->pid ] = ps;
		signal(SIGTSTP, SIG_DFL);
		kill(ps->pid, SIGTSTP);
		signal(SIGTSTP, _suspend);
		cout << "\n";
		
	}

}
std::vector<Shell::Process*>  ProcessManager::getSuspendedList(){

	std::sort(this->suspended.begin(), this->suspended.end());
	return this->suspended;
}

void ProcessManager::setForeground(int pos){
	if(pos <= 0 || pos > this->suspended.size()){
		cout << "fg: no such job\n";
		return;
	}
	std::sort(this->suspended.begin(), this->suspended.end());

	Process* ps = this->suspended.at(pos-1);
	Process* temp = ps;
	//cout << ps->pid << "\n" ;
	//printf("[DEBUG] receive signal continue , continue [%s][%d] now\n",ps->exec,ps->pid);
	cout << "Job wake up: " << ps->command << "\n";
	this->suspended.erase(this->suspended.begin()+(pos-1));
	this->running = ps;
	//signal(SIGCONT, SIG_DFL);
//	kill(ps->pid, SIGCONT);
	
	//we cont next process if it is exists
	while(temp != NULL){
		kill(temp->pid, SIGCONT);
		temp = temp->next;
	}

	temp = ps;
	while(temp != NULL){
	
		temp->handleSignals(this);
		temp = temp->next;
	}
}

void ProcessManager::execute(Process* ps){
	ps->isFrontProcess = true;
	this->running = ps;

	int pid = this->executeInternal( ps );
	
	if(pid == 0){
		return;
	}

	/*
	int pid = ps->execute( this );
	if(pid == 0){
		return;
	}
	//this->running = ps;
	//this->fg = pid;
	this->processes[ pid ] = ps;*/
}
int ProcessManager::executeInternal(Process* ps){

	static int order = 0;
	ps->time = order++;
	int pid = ps->execute( this );
	if(pid == 0){
		return 0;
	}
	this->processes[ pid ] = ps;
	return pid;
}
