#ifndef PROCESS_H
#include <map>
#include "command.h"

#define PROCESS_H


namespace Shell{
	class ProcessManager;
	class Process{
	public:
		int pid;
		int time;
		char* command;
		
		char* cmd;
		char* exec;
		std::vector<token> tokens;
		char** arguments;
		Process* previous;	
		Process* next;	

		char* input;
		char* output;
		int outMode;
		int pipeIn;
		int pipeOut;
		bool isFrontProcess;
		Process();
		int execute(ProcessManager* pm);
		void handleSignals(ProcessManager* pm);
		bool operator< (const Process &other) const {
			return time < other.time;
		}
	protected:

	};

	class ProcessManager{
	public:
		Shell::Process* running;
		
		ProcessManager();

		void execute(Shell::Process* ps);
		int executeInternal(Shell::Process* ps);

		void setForeground(int pid);
		std::vector<Shell::Process*>  getSuspendedList();
		void suspend(int sig);
		void handleSuspend(Process* ps);
		void handleExit(Process* ps);
		protected:
		std::map<int , Shell::Process*> processes;
		std::vector<Shell::Process*> suspended;
		int shellPID;
	};
}
#endif
